#!/usr/bin/env bash

echo "Starting newman with variables..."

newman \
	run ./taxonomy-tests.json \
	-e dev.postman_environment.json \
	--env-var client_id=$CLIENT_ID \
	--env-var client_secret=$CLIENT_SECRET \
	--env-var id_gen_key=$ID_GEN_API_KEY \
